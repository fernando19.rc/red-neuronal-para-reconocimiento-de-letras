# RED NEURONAL PARA RECONOCIMIENTO DE DIGITOS Y LETRAS

Se realiza una aplicación para el reconocimiento de:


- [X] DIGITOS

- [X] LETRAS

Mediante una Convolution Neural Network (CNN) programada en Python con el uso de Keras.

Se desarrolla de igual forma una aplicación grafica que permite la demostración del modelo generado.