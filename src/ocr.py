import cv2
import pytesseract
import numpy as np
from tkinter import *
import tkinter as tk
from tkinter.ttk import *
import tkinter.filedialog
import win32gui
import os

def OCR(file):
	image = cv2.imread(file.name);
	gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY);
	gray, image_bin = cv2.threshold(gray,128,255,cv2.THRESH_BINARY | cv2.THRESH_OTSU);
	gray = cv2.bitwise_not(image_bin);
	kernel = np.ones((2, 1), np.uint8);
	image = cv2.erode(gray, kernel, iterations=1);
	image = cv2.dilate(image, kernel, iterations=1);
	text = pytesseract.image_to_string(image);
	text = text[0 : len(text) - 1];
	return text;

class App(tk.Tk):
	def __init__(self):
		tk.Tk.__init__(self)

		self.x = self.y = 0

		# Inicializar elementos
		self.labelTitle = tk.Label(self, anchor="nw", justify="left", text="Transcripción:", font=("Helvetica", 48, "bold"), height=1, width=20)
		self.label = tk.Label(self, anchor="nw", justify="left", text="", font=("Helvetica", 48), height=10, width=20)
		self.button_file = tk.Button(self, text = "Archivo", command = self.file, height=4, width=10)
		self.button_folder = tk.Button(self, text = "Carpeta", command = self.folder, height=4, width=10)

		# Organizar elementos
		self.labelTitle.grid(row=0, column=0,pady=2, padx=2)
		self.label.grid(row=1, column=0,pady=2, padx=2)
		self.button_file.grid(row=2, column=0, pady=2)
		self.button_folder.grid(row=3, column=0, pady=2)

	def file(self):
		file = filedialog.askopenfile(mode ='r', filetypes =[('Images', '*.png')]) 
		if file is not None:
			text = OCR(file)
			self.label.configure(text=str(text))
			
	def folder(self):
		folder = filedialog.askdirectory()
		if os.path.exists(folder):
			folder_ocr = folder + "-OCR"
			if not os.path.exists(folder_ocr):
				os.makedirs(folder_ocr)
			for file_name in os.listdir(folder):
				if file_name.endswith(".png"):
					file = open(folder + "/" + file_name)
					text = OCR(file)
					file_ocr = open(folder_ocr + "/" + file_name[0:-4] + ".txt", "w+")
					file_ocr.write(text)

def main():
	app = App()
	mainloop()