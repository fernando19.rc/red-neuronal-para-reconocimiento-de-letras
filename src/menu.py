from model import Model_Generation
import sys
import predictor
import updater
import ocr

def printMenu():
	print("****************************************")
	print("*      INTELIGENCIA ARTIFICIAL II      *")
	print("*               GRUPO 4                *")
	print("*        RECONOCEDOR DE DIGITOS        *")
	print("*               Y LETRAS               *")
	print("****************************************")
	print("\n")
	print("****************************************")
	print("*             INTEGRANTES              *")
	print("* Tte. Ing. Ramirez Cuellar Fernando   *")
	print("* Est. Morales Meneses Ruben Arturo    *")
	print("* Est. Ojopi Meyer Christian Andrés    *")
	print("* Est. Stephanie Melva Garcia Vega     *")
	print("* Est. Juan Pablo Barra Yucra          *")
	print("* Est. Adilson Einar Heredia Cayo      *")

	
	print("****************************************")
	print("\n\n")
	
def main():
	if(len(sys.argv) > 1 and sys.argv[1] == '--update'):
		updater.Update();
	elif(len(sys.argv) > 1 and sys.argv[1] == '--digit'):
		printMenu()
		while True:
			opcion = input("Que operacion desea realizar? [G]eneracion del modelo | [P]rediccion del modelo: ")
			if (opcion == "G"):
				Generate = Model_Generation()
			if (opcion == "P"):
				predictor.main()
	elif(len(sys.argv) > 1 and sys.argv[1] == '--ocr'):
		printMenu()
		while True:
			ocr.main()
	elif(len(sys.argv) > 1 and sys.argv[1] == '--help'):
		printMenu()
		print("--update : Actualiza el programa a la ultima version")
		print("--digit  : Ingresa al modo para reconocer digitos")
		print("--ocr    : Ingresa al modo para reconocer texto")
		print("--help   : Muestra la ayuda")
	
if __name__ == "__main__":
	main()