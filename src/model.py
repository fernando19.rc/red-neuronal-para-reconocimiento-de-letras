import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras import backend as K

class Model_Generation():
	def __init__(self):
		(x_train, y_train), (x_test, y_test) = mnist.load_data()
		self.x_train = x_train
		self.y_train = y_train
		self.x_test = x_test
		self.y_test = y_test
		self.num_classes = 10
		self.cleanData()
		self.modeling()
		
	def cleanData(self):
		# Preparar los datos
		self.x_train = self.x_train.reshape(self.x_train.shape[0], 28, 28, 1)
		self.x_test = self.x_test.reshape(self.x_test.shape[0], 28, 28, 1)

		# Convertir vectores en matrices binarias
		self.y_train = keras.utils.to_categorical(self.y_train, self.num_classes)
		self.y_test = keras.utils.to_categorical(self.y_test, self.num_classes)

		# Limpiar los datos
		self.x_train = self.x_train.astype('float32')
		self.x_test = self.x_test.astype('float32')
		self.x_train /= 255
		self.x_test /= 255

	def modeling(self):
		# Configurar tamaños y cantidad de epocas (mayor es mejor)
		batch_size = 128
		epochs = 10
		input_shape = (28, 28, 1)

		# Compilar modelo de Keras
		model = Sequential()
		model.add(Conv2D(32, kernel_size=(3, 3),activation='relu',input_shape=input_shape))
		model.add(Conv2D(64, (3, 3), activation='relu'))
		model.add(MaxPooling2D(pool_size=(2, 2)))
		model.add(Dropout(0.25))
		model.add(Flatten())
		model.add(Dense(256, activation='relu'))
		model.add(Dropout(0.5))
		model.add(Dense(self.num_classes, activation='softmax'))
	
		model.compile(loss=keras.losses.categorical_crossentropy,optimizer=keras.optimizers.Adadelta(),metrics=['accuracy'])

		# Realizar el entrenamiento
		hist = model.fit(self.x_train, self.y_train,batch_size=batch_size,epochs=epochs,verbose=1,validation_data=(self.x_test, self.y_test))
		print("\n\n")
		print("****************************************")
		print("*       Entranamiento finalizado       *")
		print("****************************************")

		# Guardar modelo
		model.save('model.h5')

		# Evaluar modelo
		value = model.evaluate(self.x_test, self.y_test, verbose=0)
		print('Perdidas en las pruebas:', value[0])
		print('Precision en las pruebas:', value[1])