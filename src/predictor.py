from keras.models import load_model
from tkinter import *
import tkinter as tk
from tkinter.ttk import *
from tkinter.filedialog import askopenfile 
import win32gui
from PIL import ImageGrab, Image, ImageOps
import numpy as np

def predecir(img):
	global model
	# Ajustar imagenes a 28x28 pixeles
	img = img.resize((28,28))
	
	# Convertir imagenes RGB a BW
	img = img.convert('L')
	img = np.array(img)
	
	# Limpiar imagen para el modelo y normalizar
	img = img.reshape(1,28,28,1)
	img = img/255.0
	
	# Realizar la predicción
	res = model.predict([img])[0]
	return np.argmax(res), max(res)

class App(tk.Tk):
	def __init__(self):
		tk.Tk.__init__(self)

		self.x = self.y = 0

		# Inicializar elementos
		self.canvas = tk.Canvas(self, width=300, height=300, bg = "white", cursor="cross")
		self.label = tk.Label(self, text="...", font=("Helvetica", 48))
		self.classify_btn = tk.Button(self, text = "Reconocer", command = self.clasificar) 
		self.button_clear = tk.Button(self, text = "Limpiar", command = self.limpiar)
		self.button_file = tk.Button(self, text = "Archivo", command = self.archivo)

		# Organizar elementos
		self.canvas.grid(row=0, column=0, pady=2, sticky=W, )
		self.label.grid(row=0, column=1,pady=2, padx=2)
		self.classify_btn.grid(row=1, column=1, pady=2, padx=2)
		self.button_clear.grid(row=1, column=0, pady=2)
		self.button_file.grid(row=1, column=2, pady=2)
		
		# Evento de pintar
		self.canvas.bind("<B1-Motion>", self.dibujar)

	def archivo(self):
		file = askopenfile(mode ='r', filetypes =[('Images', '*.png')]) 
		if file is not None:
			print(file) 
			print(file.name)
			im = Image.open(file.name)
			inverted_image = ImageOps.invert(im)
			digit, acc = predecir(inverted_image)
			self.label.configure(text= "#: " + str(digit)+'\n Acc.: '+ str(int(acc*100))+'%')

	def limpiar(self):
		self.canvas.delete("all")

	def clasificar(self):
		HWND = self.canvas.winfo_id()
		rect = win32gui.GetWindowRect(HWND)
		im = ImageGrab.grab(rect)
		inverted_image = ImageOps.invert(im)
		digit, acc = predecir(inverted_image)
		self.label.configure(text= "#: " + str(digit)+'\n Acc.: '+ str(int(acc*100))+'%')

	def dibujar(self, event):
		self.x = event.x
		self.y = event.y
		r=8
		self.canvas.create_oval(self.x-r, self.y-r, self.x + r, self.y + r, fill='black')

def main():
	global model
	try:
		model = load_model('model.h5')
		app = App()
		mainloop()
	except:
		print("No existe el modelo model.h5, realice la generacion primero")