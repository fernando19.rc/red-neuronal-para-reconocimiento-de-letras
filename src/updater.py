import os, git, shutil

def Update():
	os.system('rmdir /S /Q "{}"'.format("./temp"));
	git.Repo.clone_from('https://gitlab.com/fernando19.rc/red-neuronal-para-reconocimiento-de-letras', './temp', branch='master');
	
	tempPath = "./temp";
	srcPath = "./temp/src";
	files = os.listdir(srcPath);
	for file in files:
		if(file[0] != '.'):
			shutil.copy(srcPath + "/" + file, "./" + file);
		
	os.system('rmdir /S /Q "{}"'.format("./temp"));
	print("Actualizacion exitosa");